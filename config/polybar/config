;==========================================================
;
;
;   ██████╗  ██████╗ ██╗  ██╗   ██╗██████╗  █████╗ ██████╗
;   ██╔══██╗██╔═══██╗██║  ╚██╗ ██╔╝██╔══██╗██╔══██╗██╔══██╗
;   ██████╔╝██║   ██║██║   ╚████╔╝ ██████╔╝███████║██████╔╝
;   ██╔═══╝ ██║   ██║██║    ╚██╔╝  ██╔══██╗██╔══██║██╔══██╗
;   ██║     ╚██████╔╝███████╗██║   ██████╔╝██║  ██║██║  ██║
;   ╚═╝      ╚═════╝ ╚══════╝╚═╝   ╚═════╝ ╚═╝  ╚═╝╚═╝  ╚═╝
;
;
;   To learn more about how to configure Polybar
;   go to https://github.com/polybar/polybar
;
;   The README contains a lot of information
;
;==========================================================

[colors]
; {{{
; background = ${xrdb:background:#282a36}
background = #000031
foreground = ${xrdb:foreground:#f8f8f2}
foreground-alt = ${xrdb:background:#282a36}
black = ${xrdb:color0:#000000}
blackLight = ${xrdb:color8:#4d4d4d}
; red = ${xrdb:color1:#ff5555}
red = #930036
redLight = ${xrdb:color9:#ff6e67}
green = ${xrdb:color2:#50fa7b}
greenLight = ${xrdb:color10:#5af78e}
yellow = ${xrdb:color3:#f1fa8c}
yellowLight = ${xrdb:color11:#f4f99d}
blue = ${xrdb:color4:#bd93f9}
blueLight = ${xrdb:color12:#caa9fa}
; magenta = ${xrdb:color5:#ff79c6}
magenta = #FD5402
; magentaLight = ${xrdb:color13:#ff92d0}
magentaLight = #FE9C01
cyan = ${xrdb:color6:#f8be9fd}
; cyanLight = ${xrdb:color14:#9aedfe}
cyanLight = #A5ED8B
white = ${xrdb:color7:#bfbfbf}
whiteLight = ${xrdb:color15:#e6e6e6}
; }}}

[bar/primary]
; {{{
enable-ipc = true
monitor = ${env:PRIMARY_MONITOR}
; monitor = eDP
width = 100%
height = 20
;offset-x = 1%
;offset-y = 1%
radius = 0.0
fixed-center = false

background = ${colors.background}
foreground = ${colors.foreground}

line-size = 0
line-color = ${colors.foreground}

border-size = 0

padding-left = 0
padding-right = 2

module-margin-left = 2
module-margin-right = 2

font-0 = Iosevka:bold:pixelsize=8;3
font-1 = Cantarell:bold:pixelsize=8;2
font-2 = Cantarell Extra Bold:style=Extra Bold:pixelsize=10;3
font-3 = Font Awesome 5 Free Solid:style=Solid:pixelsize=10;3
font-4 = Monaco:pixelsize=9;2
font-5 = Roboto:pixelsize=9;2

modules-left = i3 xwindow
; modules-center = mpd
modules-right = uptime-pretty wallpaper pulseaudio filesystem cpu memory eth wlan battery date powermenu

tray-position = righ
tray-padding = 2

;wm-restack = bspwm
;wm-restack = i3

;override-redirect = true

;scroll-up = bspwm-desknext
;scroll-down = bspwm-deskprev

;scroll-up = = ${xrdb:wsnext
;scroll-down = = ${xrdb:wsprev

cursor-click = pointer
cursor-scroll = ns-resize

separator = %{F#434343}%{F-}
; }}}

[bar/secondary]
; {{{
enable-ipc = true
monitor = ${env:SECONDARY_MONITOR}
; monitor = DisplayPort-0
width = 100%
height = 27
;offset-x = 1%
;offset-y = 1%
radius = 0.0
fixed-center = false

background = ${colors.background}
foreground = ${colors.foreground}

line-size = 3
line-color = ${colors.foreground}

border-size = 0

padding-left = 0
padding-right = 2

module-margin-left = 2
module-margin-right = 2

font-0 = Iosevka:bold:pixelsize=8;3
font-1 = Cantarell:bold:pixelsize=8;2
font-2 = Cantarell Extra Bold:style=Extra Bold:pixelsize=10;3
font-3 = Font Awesome 5 Free Solid:style=Solid:pixelsize=10;3
font-4 = Monaco:pixelsize=8;2
font-5 = Roboto:pixelsize=8;2

modules-left = i3 xwindow
; modules-center = mpd

tray-position = none
tray-padding = 2

;wm-restack = bspwm
;wm-restack = i3

;override-redirect = true

;scroll-up = bspwm-desknext
;scroll-down = bspwm-deskprev

;scroll-up = = ${xrdb:wsnext
;scroll-down = = ${xrdb:wsprev

cursor-click = pointer
cursor-scroll = ns-resize

separator = %{F#434343}%{F-}
; }}}

[module/xwindow]
; {{{
type = internal/xwindow
label-padding = 2
label-foreground = ${colors.foreground}
label = %title:0:80:...%
; }}}

[module/uptime-pretty]
; {{{
type = custom/script
exec = ~/.config/polybar/uptime.sh
interval = 30
; }}}

[module/wallpaper]
; {{{
type = custom/text
content-padding = 1
content-foreground = ${colors.foreground}
content = 
click-left = "feh --randomize --bg-scale ~/Pictures/wallpapers/* &"
; }}}

[module/filesystem]
; {{{
type = internal/fs
interval = 25

mount-0 = /

label-mounted =  %percentage_used:2%%
label-unmounted = %mountpoint% not mounted
label-unmounted-foreground = ${colors.foreground-alt}
; }}}

[module/i3]
; {{{
type = internal/i3
format = <label-state> <label-mode>
pin-workspaces = true
index-sort = true
wrapping-scroll = false

label-dimmed-underline = ${root.background}

label-mode = %mode%
label-mode-padding = 1
label-mode-font = 6
label-mode-foreground = ${colors.black}
label-mode-background = ${colors.yellow}

label-focused = %name% 
label-focused-foreground = ${colors.magentaLight}
label-focused-background = ${colors.background}
label-focused-underline = ${colors.magentaLight}
label-focused-font = 0
label-focused-padding = 1

label-unfocused = %name%
label-unfocused-foreground = ${colors.foreground}
label-unfocused-background = ${colors.background}
;label-unfocused-overline = #727272
;label-unfocused-underline = #727272
label-unfocused-font = 0
label-unfocused-padding = 1

label-urgent = %name%
label-urgent-foreground = ${colors.white}
label-urgent-background = ${colors.red}
;label-urgent-overline = #ffb52a
;label-urgent-underline = #FFB52A
label-urgent-font = 0
label-urgent-padding = 1

label-visible = %name%
label-visible-foreground = ${colors.foreground}
label-visible-font = 0
label-visible-padding = 1
; }}}

[module/mpd]
; {{{
type = internal/mpd
format-online = <label-song>  <icon-prev> <icon-stop> <toggle> <icon-next>

icon-prev = 
icon-stop = 
icon-play = 
icon-pause = 
icon-next = 

label-song-maxlen = 25
label-song-ellipsis = true
; }}}

[module/backlight-acpi]
; {{{
type = internal/backlight
format = <label> <bar>
label = 
card = amdgpu_bl0

enable-scroll = true

bar-width = 10
bar-indicator = І
bar-indicator-foreground = ${colors.foreground}
bar-indicator-font = 3
bar-fill = І
bar-fill-font = 3
bar-fill-foreground = ${colors.foreground}
bar-empty = І
bar-empty-font = 3
bar-empty-foreground = ${colors.blackLight}
; }}}

[module/cpu]
; {{{
type = internal/cpu

interval = 1

; Available tags:
;   <label> (default)
;   <bar-load>
;   <ramp-load>
;   <ramp-coreload>
format = <label>

; Available tokens:
;   %percentage% (default) - total cpu load
;   %percentage-cores% - load percentage for each core
;   %percentage-core[1-9]% - load percentage for specific core
label = CPU %percentage:2%%
; }}}

[module/memory]
; {{{
type = internal/memory
interval = 2
format = <label>
label = RAM %percentage_used:2%%
; }}}

[module/wlan]
; {{{
type = internal/network
interface = wlo1
interval = 3.0

format-connected = <label-connected>
label-connected =  %essid% %local_ip%

format-disconnected = N/A
;format-disconnected = <label-disconnected>
;format-disconnected-underline = ${self.format-connected-underline}
;label-disconnected = %ifname% disconnected
;label-disconnected-foreground = ${colors.foreground-alt}
; }}}

[module/eth]
; {{{
type = internal/network
interface = enp89s0
interval = 3.0

format-connected-underline = #55aa55
format-connected-prefix = " "
format-connected-prefix-foreground = ${colors.foreground-alt}
label-connected = %local_ip%

format-disconnected =
;format-disconnected = <label-disconnected>
;format-disconnected-underline = ${self.format-connected-underline}
;label-disconnected = %ifname% disconnected
;label-disconnected-foreground = ${colors.foreground-alt}
; }}}

[module/date]
; {{{
type = internal/date
interval = 1

date = "%Y-%m-%d %a "
date-alt = ""

time = %H:%M
time-alt = %H:%M:%S

label = %date%%time%
; }}}

[module/pulseaudio]
; {{{
type = internal/pulseaudio

format-volume = <label-volume> <bar-volume>
label-volume = VOL %percentage%%
label-volume-foreground = ${root.foreground}

label-muted = 🔇 muted
label-muted-foreground = #666

bar-volume-width = 10
bar-volume-foreground-0 = #55aa55
bar-volume-foreground-1 = #55aa55
bar-volume-foreground-2 = #55aa55
bar-volume-foreground-3 = #55aa55
bar-volume-foreground-4 = #55aa55
bar-volume-foreground-5 = #f5a70a
bar-volume-foreground-6 = #ff5555
bar-volume-gradient = false
bar-volume-indicator = |
bar-volume-indicator-font = 2
bar-volume-fill = ─
bar-volume-fill-font = 2
bar-volume-empty = ─
bar-volume-empty-font = 2
bar-volume-empty-foreground = ${colors.foreground-alt}
; }}}

[module/alsa]
; {{{
type = internal/alsa

format-volume = <label-volume> <bar-volume>
label-volume = VOL
label-volume-foreground = ${root.foreground}

format-muted-prefix = " "
format-muted-foreground = ${colors.foreground-alt}
label-muted = sound muted

bar-volume-width = 10
bar-volume-foreground-0 = #55aa55
bar-volume-foreground-1 = #55aa55
bar-volume-foreground-2 = #55aa55
bar-volume-foreground-3 = #55aa55
bar-volume-foreground-4 = #55aa55
bar-volume-foreground-5 = #f5a70a
bar-volume-foreground-6 = #ff5555
bar-volume-gradient = false
bar-volume-indicator = |
bar-volume-indicator-font = 2
bar-volume-fill = ─
bar-volume-fill-font = 2
bar-volume-empty = ─
bar-volume-empty-font = 2
bar-volume-empty-foreground = ${colors.foreground-alt}
; }}}

[module/battery]
; {{{
type = internal/battery
battery = BAT0
adapter = AC
full-at = 96

format-charging = <animation-charging> <label-charging>

format-discharging = <ramp-capacity> <label-discharging>

format-full-prefix = "  "

label-charging = %percentage%%
label-discharging = %percentage%%

ramp-capacity-0 = 
ramp-capacity-1 = 
ramp-capacity-2 = 
ramp-capacity-3 = 
ramp-capacity-4 = 

animation-charging-0 =  
animation-charging-1 =  
animation-charging-2 =  
animation-charging-3 =  
animation-charging-4 =  
animation-charging-framerate = 750
; }}}

[module/temperature]
; {{{
type = internal/temperature
thermal-zone = 0
interval = 1
base-temperature = 20
warn-temperature = 60

format = <ramp> <label>
;format-overline = #f50a4d
format-warn = <ramp> <label-warn>
;format-warn-overline = ${self.format-underline}

label = %temperature-c%
label-warn = %temperature-c%
label-warn-foreground = ${colors.red}

ramp-0 = 
ramp-1 = 
ramp-2 = 
; ramp-foreground = ${colors.foreground-alt}
; }}}

[module/powermenu]
; {{{
type = custom/menu

expand-right = true

format-spacing = 1

label-open = 
label-open-foreground = ${colors.red}
label-close =  CANCEL
label-close-foreground = ${colors.green}
label-separator = 
label-separator-foreground = ${colors.foreground-alt}

menu-0-0 =  LOGOUT
menu-0-0-exec = menu-open-1
menu-0-1 =  REBOOT
menu-0-1-exec = menu-open-2
menu-0-2 =  POWER OFF
menu-0-2-exec = menu-open-3

menu-1-0 =  LOGOUT
menu-1-0-exec = i3-msg exit

menu-2-0 =  REBOOT
menu-2-0-exec = systemctl reboot

menu-3-0 =  POWER OFF
menu-3-0-exec = systemctl poweroff
; }}}

[settings]
; {{{
screenchange-reload = true
;compositing-background = xor
;compositing-background = screen
;compositing-foreground = source
;compositing-border = over
;pseudo-transparency = false
; }}}

[global/wm]
; {{{
margin-top = 0
margin-bottom = 0
; }}}

; vim:ft=dosini fdm=marker
